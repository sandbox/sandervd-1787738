Introduction
------------

Quickfields adds a checkbox on the 'Manage fields'-tab of field ui. 
The checkbox disables the redirect which normally happens after the creation
of a new field, or after adding an existing field.
The module is useful at the start of a new project,
when many fields are added, and the default settings are just fine.

!!! Please note: Once you have values in the database for a field, you won't be able to edit the configuration. !!!

Installation
------------

Enable the module under admin/modules

Requirements
------------

Field UI
